#!/usr/bin/env bash
#
# Author: Anton Müller <email@antonmueller.xyz>
#
# This script is generates user-accounts on a ucs machine based on a yaml file named usernames.yaml in the same directory.
# Usernames contains a list of a undefined number of Lastname and this script converts german names into ascii-only names.

usernames=$(sed '1d;s/- //g;s/ö/oe/g;s/ü/ue/g;s/ä/ae/g;s/ß/ss/g;s/[A-Z]/\L&/g' usernames.yaml)
# converting german umlauts + ß into their ascii equivalents
host=amnews
tld=local
password="Un1v3nt!0n"

for username in $usernames ;
do
	export lastname=$(grep -i $(sed 's/oe/ö/g;s/ue/ü/g;s/ae/ä/g;s/ss/ß/g' <<< $username ) usernames.yaml| sed 's/- //g' ) 
	# define lastname based on the username by reverse converting german umlauts back on the username
	univention-directory-manager users/user create --set username="$username" --set lastname="$lastname" --set password="$password" --position "cn=users,dc=$host,dc=$tld"
	# generate the user with $username as username with predefined $password and previous defined $lastname in ldap-container/positin cn=user of amnews.local
	echo "## the user $username for $lastname is created. ##"
	# telling the user its created
done;
