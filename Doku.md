## 1. Installation von Univention Corporate Server
*Als Testsystem kannst du eine virtuelle Maschine unserer "Core Edition" einsetzen.
Die Installation kann auf Hardware oder in einer virtuellen Maschine erfolgen (z.B. VirtualBox oder VMware Player).*

- Virtualbox VM mit statischer IP

## 2. Schnittstellen unseres Produktes

### 2.1 Testaccounts generieren
*In der Produktdokumentation ist die Kommandozeilen-/Skriptschnittstelle des Univention Directory Managers (UDM) beschrieben. Bitte lege über die Kommandozeile 100 Testaccounts (Benutzerobjekte) im Managementsystem an.*

- Script [1] mit Iteration via forschleife über 

 ```
 $ univetion-directory-manager users/user create \
 		--set username="$username" \
 		--set lastname="$lastname" \
 		--set password="$password" \
 		--position "cn=users,dc=$host,dc=$tld"
 ```

- $username wurde aus einer Liste mit 100 deutschen Namen generiert (Umlaute wurden umgeschrieben) und via for-schleife aus $usernames genommen
- $lastname ist aus $username generiert.
- $password, $host, $tld ist jeweils predefined

## 2.2 Testaccount modifizieren
*Im Weiterem soll einer der Benutzer erweiterte Rechte erhalten, so dass mit diesem Benutzer eine Anmeldung per Web-Browser an der Verwaltungsoberfläche Univention Management Console möglich ist, um dort Verwaltungsaufgaben vorzunehmen.*

```
$ univention-directory-manager groups/group modify \
    --dn "cn=Domain Admins,cn=groups,dc=amnews,dc=local" \
    --append users="uid=amueller,cn=users,dc=amnews,dc=local"
```

### 2.3 Testaccounts prüfen
*Bitte nutze die Standardtools unabhängig von UDM (z.B. ldapsearch/univention-ldapsearch), um das erfolgreiche Anlegen der Testaccounts zu verifizieren.*

```
ldapsearch -x -h ucs.amnews.local \ 
  -D "uid=Administrator,cn=users,dc=amnews,dc=local" -w "$adminpw" \
  -b "cn=users,dc=amnews,dc=local" -u uid
```

$adminpw wurde vor exportiert


## 3. Installation weiterer Software
*In der Produktdokumentation ist beschrieben wie neben Standardpaketen auch weitere Software ohne Maintenance installiert werden kann. Installiere bitte Google Chrome oder Chromium.* 

```
univention-install chromium
```

## 4. Technische Dokumentation
*Häufig wird man sich mit Kollegen oder Kunden absprechen wollen. Wähle ein geeignetes Format, um die vorherigen Ergebnisse auf verständliche Weise zu dokumentieren. Die Zielgruppe sind also Techniker, die zwar Bash-Scripte lesen können, aber vielleicht nicht jeden Befehl kennen.*

- Markdown in PDF konvertiert

---
| Autor: | [Anton Müller](mailto:email@antonmueller.xyz)|
|  ---   | :---: |
| Stand: | 2021-12-23 |
| git: | [git.dezentrale.cloud](https://git.dezentrale.cloud/therojam/testaufgabe) 
